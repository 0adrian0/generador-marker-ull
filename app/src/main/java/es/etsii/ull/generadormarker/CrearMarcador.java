package es.etsii.ull.generadormarker;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import es.ava.aruco.Marker;

/**
 * Clase que gestiona la creación de los marcadores de los alumnos
 */
public class CrearMarcador extends Activity{

	// layout fields
	private EditText mMarkerId;
	private ImageView newMarkerView;
	private Button mSaveButton;

    private int borde;
    private int anchoSubMarker;
    private int anchoMarker;

    public CrearMarcador() {
        anchoMarker = 475;
        borde = 25;
        anchoSubMarker = 200;
    }

    /** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.new_marker);
		
		// get the layout fields
		newMarkerView = (ImageView) findViewById(R.id.newMarker);

        Intent intent = getIntent();
        String niu = intent.getStringExtra("niu");
        createMarker(niu);
	}

    // Cuando presione la tecla atrás le pregunto si quiere salir
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            AlertDialog.Builder alert = new AlertDialog.Builder(CrearMarcador.this);

            alert.setTitle("Notificación de guardado");
            alert.setMessage("¿Desea guardar este marcador?");

            alert.setPositiveButton("Si", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    dialog.dismiss();
                    AlertDialog.Builder alert = new AlertDialog.Builder(CrearMarcador.this);

                    alert.setTitle("Notificación de guardado");
                    alert.setMessage("Marcador guardado. Podrá verlo pulsando el botón \"Ver Marcador Guardado\".");

                    alert.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            saveMarker();
                            dialog.dismiss();
                            finish();
                            overridePendingTransition(R.anim.right_in,R.anim.right_out);
                        }
                    });

                    alert.show();
                }
            });

            alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    dialog.dismiss();
                    finish();
                    overridePendingTransition(R.anim.right_in,R.anim.right_out);
                }
            });

            alert.show();
        }
        return false;
    }

    private  char[] codifyRotations (int numero) {
        String numBin = Integer.toString(numero, 2);
        int tamanio = numBin.length();
        String bitsFaltantes = "";
        if (tamanio < 8) {
            for (int i = 0; i < 8 - tamanio; i++) {
                bitsFaltantes += "0";
            }
            numBin = bitsFaltantes + numBin;
        }
        char[] numBinArray = numBin.toCharArray();
        return numBinArray;
    }
	
	/** Buttons actions */
	public void createMarker(String niu) {
		try {

            char[] arrayAlu = niu.toCharArray();

            Bitmap b_img = Bitmap.createBitmap(660, 680, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(b_img);
            canvas.drawRGB(250, 250, 250);



            int cont = 1;
            String bitsRot = null;
            char[] numBinArray = codifyRotations(Integer.parseInt(Character.toString(arrayAlu[0]).concat(Character.toString(arrayAlu[1]))));

            int punt = 7;
            for (int i = 0; i < arrayAlu.length; i += 2) {
                if (i != 0) {
                    Mat marker = Marker.createMarkerImage(Integer.parseInt(String.valueOf(cont) + arrayAlu[i] + arrayAlu[i + 1]), anchoSubMarker);
                    Bitmap markerImg = Bitmap.createBitmap(marker.cols(), marker.rows(), Bitmap.Config.ARGB_8888);
                    Utils.matToBitmap(marker, markerImg);

                    float[] mirrorY = { -1, 0, 0, 0, 1, 0, 0, 0, 1 };
                    Matrix matrixMirror = new Matrix();
                    matrixMirror.setValues(mirrorY);
                    Matrix matrix = new Matrix();
                    matrix.postConcat(matrixMirror);
                    matrix.postRotate(-45);


                    bitsRot = Character.toString(numBinArray[i - 2]).concat(Character.toString(numBinArray[i - 1]));
                    if (bitsRot.equalsIgnoreCase("01")) {
                        matrix.postRotate(-90);
                    } else if (bitsRot.equalsIgnoreCase("10")) {
                        matrix.postRotate(-180);
                    } else if (bitsRot.equalsIgnoreCase("11")) {
                        matrix.postRotate(-270);
                    }


                    // create a new bitmap from the original using the matrix to transform the result
                    Bitmap rotatedBitmap = Bitmap.createBitmap(markerImg , 0, 0, markerImg.getWidth(), markerImg.getHeight(), matrix, true);

                    if (cont == 1) {

                        canvas.drawBitmap(rotatedBitmap, 190, borde, null);
                    }
                    else if (cont == 2) {
                        canvas.drawBitmap(rotatedBitmap, borde, 190, null);
                    }
                    else if (cont == 3) {
                        canvas.drawBitmap(rotatedBitmap, 360, 190, null);
                    }
                    else if (cont == 4) {
                        canvas.drawBitmap(rotatedBitmap, 190, 360, null);
                    }
                    cont++;
                    Imgproc.cvtColor(marker, marker, Imgproc.COLOR_GRAY2RGBA, 4);
                }
            }

            Paint paint = new Paint();

            paint.setStrokeWidth(4);
            paint.setColor(Color.BLACK);
            paint.setStyle(Paint.Style.FILL_AND_STROKE);
            paint.setAntiAlias(true);

            Point a = new Point(40, 400);
            Point b = new Point(280, 650);
            Point c = new Point(40, 650);

            Path path = new Path();
            path.setFillType(Path.FillType.EVEN_ODD);
            path.setLastPoint(a.x, a.y);
            path.lineTo(b.x, b.y);
            path.lineTo(c.x, c.y);
            path.lineTo(a.x, a.y);
            path.close();

            canvas.drawPath(path, paint);

            a = new Point(630, 400);
            b = new Point(380, 650);
            c = new Point(630, 650);

            path = new Path();
            path.setFillType(Path.FillType.EVEN_ODD);
            path.setLastPoint(a.x, a.y);
            path.lineTo(b.x, b.y);
            path.lineTo(c.x, c.y);
            path.lineTo(a.x, a.y);
            path.close();

            canvas.drawPath(path, paint);

			newMarkerView.setImageBitmap(b_img);
		} catch(Exception e) {
			// TODO position the toast
            new AlertDialog.Builder(CrearMarcador.this)
                    .setTitle("Error")
                    .setMessage("Ha introducido un NIU erróneo.")
                    .setPositiveButton("Volver", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                            dialog.dismiss();
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
            Log.d(">>> ", e.toString());
		}
	}
	
	public void saveMarker(){
		  File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
		  File file = new File(path, "marker.png");// TODO add time mark to name
		  path.mkdirs();// make sure the picture dir exists
		  FileOutputStream os = null;

		  try {
			  os = new FileOutputStream(file);
			  newMarkerView.buildDrawingCache();
			  newMarkerView.getDrawingCache().compress(Bitmap.CompressFormat.PNG, 0, os);

              //Guarda el marcador en la carpeta Pictures
              Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
              intent.setData(Uri.fromFile(file));
              sendBroadcast(intent);

		  } catch (FileNotFoundException fnfe) {

		  } finally {
			  if (os != null) {
				  try {
					  os.close();
				  } catch (IOException ioe) {
					  // do nothing
				  }
			  }
		  }
	}
	
	private void showToast(String message){
		Context context = getApplicationContext();
		int duration = Toast.LENGTH_SHORT;
		Toast toast = Toast.makeText(context, message, duration);
		toast.show();
	}
}
