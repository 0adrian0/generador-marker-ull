package es.etsii.ull.generadormarker;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Editable;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

/**
 * Clase que gestiona la ventana principal donde se muestran las dos opciones disponibles
 */
public class MainActivity extends ActionBarActivity {
    private Context contexto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        contexto = MainActivity.this;

        Button bt = (Button) findViewById(R.id.buttonGenerar);
        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder alert = new AlertDialog.Builder(contexto);

                alert.setTitle("Niu requerido");
                alert.setMessage("Por favor, introduzca un NIU para generar un marcador.");

                // Set an EditText view to get user input
                final EditText input = new EditText(contexto);
                alert.setView(input);

                alert.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        InputMethodManager imm = (InputMethodManager)getSystemService(
                                Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
                        Editable value = input.getText();
                        if (value.toString().length() == 10) {
                            Intent i = new Intent(contexto, CrearMarcador.class);
                            i.putExtra("niu", value.toString());
                            i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                            dialog.dismiss();
                            startActivity(i);
                            overridePendingTransition(R.anim.right_in,R.anim.right_out);
                        } else {
                            new AlertDialog.Builder(contexto)
                                    .setTitle("Error")
                                    .setMessage("Ha introducido un NIU erróneo.")
                                    .setPositiveButton("Volver", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    })
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .show();
                        }
                    }
                });

                alert.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                });

                alert.show();
            }
        });

        bt = (Button) findViewById(R.id.buttonVer);
        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(contexto, VerMarcadorGuardado.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                startActivity(i);
                overridePendingTransition(R.anim.right_in,R.anim.right_out);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
