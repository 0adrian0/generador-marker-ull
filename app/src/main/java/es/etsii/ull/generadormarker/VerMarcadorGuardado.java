package es.etsii.ull.generadormarker;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.view.KeyEvent;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import java.io.File;

/**
 * Clase que muestra el marcador guardado previamente en el dispositivo
 */
public class VerMarcadorGuardado extends Activity {

    private ImageView markerView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.view_marker);
        File imgFile = new  File("/sdcard/Pictures/marker.png");
        if(imgFile.exists()){
            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            markerView = (ImageView) findViewById(R.id.saveMarker);
            markerView.setImageBitmap(myBitmap);
        }
    }

    // Cuando presione la tecla atrás le pregunto si quiere salir
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            finish();
            overridePendingTransition(R.anim.right_in,R.anim.right_out);
        }
        return false;
    }
}
